package com.example.gifsearcher.eventlisteners;

import com.example.gifsearcher.util.AppScreen;

/**
 * Created by User-PC on 29.08.2016.
 */
public interface NavigationDrawerListener {

    void navigationTabChanged(AppScreen navigationTab);
}
