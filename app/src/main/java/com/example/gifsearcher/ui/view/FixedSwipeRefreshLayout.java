package com.example.gifsearcher.ui.view;

/**
 * Created by User-PC on 29.08.2016.
 */
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class FixedSwipeRefreshLayout extends SwipeRefreshLayout {

    private RecyclerView recyclerView;
    private ScrollView scrollView;

    public FixedSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedSwipeRefreshLayout(Context context) {
        super(context);
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public void setScrollView(ScrollView scrollView) {
        this.scrollView = scrollView;
    }

    @Override
    public boolean canChildScrollUp() {
        if (recyclerView!=null) {
            return recyclerView.canScrollVertically(-1);
        }

        if (scrollView!=null) {
            return scrollView.canScrollVertically(-1);
        }

        return false;
    }

}