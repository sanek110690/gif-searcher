package com.example.gifsearcher.ui.fragment;

import android.app.Fragment;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gifsearcher.R;
import com.example.gifsearcher.ui.activity.MainActivity;
import com.example.gifsearcher.adapter.GifsAdapter;
import com.example.gifsearcher.api.response.Response;
import com.example.gifsearcher.loader.GifsLoader;
import com.example.gifsearcher.model.Gif;
import com.example.gifsearcher.ui.view.FixedSwipeRefreshLayout;

import java.util.List;

/**
 * Created by User-PC on 29.08.2016.
 */
public class GifsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Response>, SwipeRefreshLayout.OnRefreshListener {

    public  GifsAdapter mAdapter;
    public List<Gif> gifsList;
    public static GifsFragment instance;
    private int loaderId;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gifs, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new GifsAdapter(getActivity());
        recyclerView.setAdapter(mAdapter);
        instance = this;
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        onRefresh();
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.gifs_loader:
                //showProgress();
                return new GifsLoader(getActivity());
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        loaderId = loader.getId();
        if (loaderId == R.id.gifs_loader) {
            gifsList = data.getTypedData();
            mAdapter.setGifs(gifsList);
        }
        getLoaderManager().destroyLoader(loaderId);
    }



    @Override
    public void onLoaderReset(Loader<Response> loader) {

    }

    @Override
    public void onRefresh() {
        Log.d("onRefresh:", "onRefresh");
        getLoaderManager().destroyLoader(loaderId);
        MainActivity.instance.getSupportLoaderManager().initLoader(R.id.gifs_loader, Bundle.EMPTY, this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);

            }
        }, 6000);

    }
}
