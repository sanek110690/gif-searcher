package com.example.gifsearcher.ui.activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.gifsearcher.R;
import com.example.gifsearcher.eventlisteners.NavigationDrawerListener;
import com.example.gifsearcher.model.Gif;
import com.example.gifsearcher.util.AppScreen;
import com.example.gifsearcher.ui.fragment.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 29.08.2016.
 */
public class MainActivity extends ActionBarActivity implements NavigationDrawerListener {

    private AppScreen appScreen = AppScreen.Posts;
    public static MainActivity instance;
    private GifsFragment gifsFragment = new GifsFragment();
    Toolbar toolbar;
    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_app_screen, gifsFragment);
        fragmentTransaction.commit();

    }

    @Override
    public void navigationTabChanged(AppScreen navigationTab) {
        if (navigationTab == appScreen) return;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(appScreen.name());
        if (getFragmentManager().getBackStackEntryCount() > 10) {
            getFragmentManager().popBackStack();
        }
        switch (navigationTab) {
            case Posts:
                fragmentTransaction.replace(R.id.fragment_app_screen, gifsFragment);
                break;
        }
        fragmentTransaction.commit();
        appScreen = navigationTab;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                handleMenuSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void handleMenuSearch(){
        ActionBar action = getSupportActionBar();

        if(isSearchOpened){

            action.setDisplayShowCustomEnabled(false);
            action.setDisplayShowTitleEnabled(true);


            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if(imm != null){
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            GifsFragment.instance.mAdapter.setGifs(GifsFragment.instance.gifsList);
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.search_icon));

            isSearchOpened = false;
        } else {

            action.setDisplayShowCustomEnabled(true);
            action.setCustomView(R.layout.search_bar);
            action.setDisplayShowTitleEnabled(false);

            edtSearch = (EditText)action.getCustomView().findViewById(R.id.edtSearch);

            edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();
                        return true;
                    }
                    return false;
                }
            });


            edtSearch.requestFocus();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);

            mSearchAction.setIcon(getResources().getDrawable(R.drawable.close_search));

            isSearchOpened = true;
        }
    }

    @Override
    public void onBackPressed() {
        if(isSearchOpened) {
            handleMenuSearch();
            return;
        }
        super.onBackPressed();
    }

    private void doSearch() {
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        List<Gif> searchGifsList = new ArrayList<>();
        for (Gif gif:
        GifsFragment.instance.gifsList) {
            if (gif.getSlug().indexOf(edtSearch.getText().toString()) != -1) {
                searchGifsList.add(gif);
            }
        }
        GifsFragment.instance.mAdapter.setGifs(searchGifsList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
