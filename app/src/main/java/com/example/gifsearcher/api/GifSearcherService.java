package com.example.gifsearcher.api;

import com.example.gifsearcher.api.response.ApiResponseModel;
import com.example.gifsearcher.model.Gif;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;

/**
 * Created by User-PC on 29.08.2016.
 */
public interface GifSearcherService {

    @GET("v1/gifs/trending?api_key=dc6zaTOxFJmzC")
    Call<ApiResponseModel<Gif>> getGifs();

}
