package com.example.gifsearcher.api.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by User-PC on 29.08.2016.
 */
public class ApiResponseModel<T> implements Serializable {

    @SerializedName("data")
    private List<T> data;

    public List<T> getData() {
        return data;
    }

    public static class Data {
        @SerializedName("result_code")
        private int resultCode;

        @SerializedName("result_msg")
        private String resultMessage;

        public int getResultCode() {
            return resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }
    }

}
