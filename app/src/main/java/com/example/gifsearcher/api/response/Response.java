package com.example.gifsearcher.api.response;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.gifsearcher.R;

/**
 * Created by User-PC on 29.08.2016.
 */
public class Response {

    @Nullable
    private ApiResponseModel mAnswer;

    private RequestResult mRequestResult;

    private boolean isSingleObject = true;

    public Response() {
        mRequestResult = RequestResult.ERROR;
    }

    @NonNull
    public RequestResult getRequestResult() {
        return mRequestResult;
    }

    public Response setRequestResult(RequestResult requestResult) {
        mRequestResult = requestResult;
        return this;
    }

    @Nullable
    public <T> T getTypedAnswer() {
        if (mAnswer == null) {
            return null;
        }
        //noinspection unchecked
        return (T) mAnswer;
    }

    @Nullable
    public <T> T getTypedData() {
        if (mAnswer == null || mAnswer.getData() == null) {
            return null;
        }
        //noinspection unchecked
        return (T) mAnswer.getData();
    }

    public Response setAnswer(@Nullable ApiResponseModel answer) {
        mAnswer = answer;
        return this;
    }

    public boolean isSuccess() {
        if(mRequestResult == RequestResult.ERROR || mAnswer == null || mAnswer.getData() == null || (isSingleObject && mAnswer.getData().size() == 0)) {
            return false;
        } else {
            return true;
        }
    }

    public String getErrorMessage(Context context) {
        if(mRequestResult == RequestResult.ERROR)
            return context.getString(R.string.request_failed_message);
        else
            return null;
    }

    public Response setIsSingleObject(boolean value) {
        isSingleObject = value;
        return this;
    }

    public void save(Context context) {
    }


}
