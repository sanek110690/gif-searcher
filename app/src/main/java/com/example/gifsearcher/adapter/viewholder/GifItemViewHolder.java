package com.example.gifsearcher.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gifsearcher.R;

/**
 * Created by User-PC on 29.08.2016.
 */
public class GifItemViewHolder extends RecyclerView.ViewHolder {

    private TextView slugTextView;
    private ImageView gifItemBackground;

    public GifItemViewHolder(View view) {
        super(view);
        slugTextView = (TextView) view.findViewById(R.id.slug_text_view);

        gifItemBackground = (ImageView) view.findViewById(R.id.gif_item_background);
    }

    public TextView getSlugTextView() {
        return slugTextView;
    }


    public ImageView getGifItemBackground() {
        return gifItemBackground;
    }
}
