package com.example.gifsearcher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.gifsearcher.R;
import com.example.gifsearcher.ui.activity.MainActivity;
import com.example.gifsearcher.adapter.viewholder.GifItemViewHolder;
import com.example.gifsearcher.model.Gif;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 29.08.2016.
 */
public class GifsAdapter extends RecyclerView.Adapter<GifItemViewHolder> {
    private Context mContext;
    private List<Gif> mGifList;

    public GifsAdapter(Context context) {
        mContext = context;
        mGifList = new ArrayList<>();
    }

    @Override
    public GifItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_gif, parent, false);
        return new GifItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GifItemViewHolder holder, final int position) {
        Gif gif = mGifList.get(position);
        holder.getSlugTextView().setText(gif.getSlug());
        String url = gif.getImages().getOriginal().getUrl().replaceAll("\\\\", "");
        Glide.with(MainActivity.instance).load(url).asGif().into(holder.getGifItemBackground());
    }

    @Override
    public int getItemCount() {
        if (mGifList != null)
            return mGifList.size();
        else return 0;
    }

    public void setGifs(List<Gif> gifs) {
        mGifList = gifs;
        notifyDataSetChanged();
    }
}
