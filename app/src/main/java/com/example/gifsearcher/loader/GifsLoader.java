package com.example.gifsearcher.loader;

import android.content.Context;

import com.example.gifsearcher.api.ApiFactory;
import com.example.gifsearcher.api.GifSearcherService;
import com.example.gifsearcher.api.response.ApiResponseModel;
import com.example.gifsearcher.api.response.RequestResult;
import com.example.gifsearcher.api.response.Response;
import com.example.gifsearcher.model.Gif;

import java.io.IOException;

import retrofit2.Call;

/**
 * Created by User-PC on 29.08.2016.
 */
public class GifsLoader extends BaseLoader {

    public GifsLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        GifSearcherService gifSearcherService = ApiFactory.getGifsService();
        Call<ApiResponseModel<Gif>> call = gifSearcherService.getGifs();
        ApiResponseModel<Gif> response = call.execute().body();
        return new Response()
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(response)
                .setIsSingleObject(false);
    }
}
