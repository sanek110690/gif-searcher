package com.example.gifsearcher.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.gifsearcher.api.response.RequestResult;
import com.example.gifsearcher.api.response.Response;

import java.io.IOException;

/**
 * Created by User-PC on 29.08.2016.
 */
public abstract class BaseLoader extends AsyncTaskLoader<Response> {

    private final static String TAG = "BaseLoader";

    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Response loadInBackground() {
        try {
            Response response = apiCall();
            if (response.getRequestResult() == RequestResult.SUCCESS) {
                response.save(getContext());
                onSuccess();
            } else {
                onError();
            }
            return response;
        } catch (IOException e) {
            onError();
            if(e.getMessage() != null) {
                Log.d(TAG, e.getMessage());
                e.printStackTrace();
            }
            return new Response();
        }
    }

    protected void onSuccess() {
    }

    protected void onError() {
    }

    protected abstract Response apiCall() throws IOException;
}
