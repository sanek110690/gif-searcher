package com.example.gifsearcher.util;

/**
 * Created by User-PC on 29.08.2016.
 */
public enum AppScreen {
    Posts(0);

    private int index;

    AppScreen(int _index) {
        index = _index;
    }

    public static AppScreen fromIndex(int _index) {
        for (AppScreen t : AppScreen.values()) {
            if (_index == t.index) return t;
        }
        return null;
    }
}
