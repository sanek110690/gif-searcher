package com.example.gifsearcher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User-PC on 29.08.2016.
 */
public class Images implements Serializable {

    @SerializedName("fixed_height_downsampled")
    private Original original;

    public Original getOriginal() {
        return original;
    }
}
