package com.example.gifsearcher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User-PC on 29.08.2016.
 */
public class Original implements Serializable {

    @SerializedName("url")
    private String url;

    @SerializedName("width")
    private String width;

    @SerializedName("height")
    private String height;

    @SerializedName("size")
    private String size;

    @SerializedName("webp")
    private String webp;

    @SerializedName("webp_size")
    private String webp_size;


    public String getUrl() {
        return url;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getSize() {
        return size;
    }

    public String getWebp() {
        return webp;
    }

    public String getWebp_size() {
        return webp_size;
    }
}
