package com.example.gifsearcher.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by User-PC on 29.08.2016.
 */
public class Gif implements Serializable {

    @SerializedName("type")
    private String type;

    @SerializedName("id")
    private String id;

    @SerializedName("slug")
    private String slug;

    @SerializedName("url")
    private String url;

    @SerializedName("bitly_gif_url")
    private String bitly_gif_url;

    @SerializedName("bitly_url")
    private String bitly_url;

    @SerializedName("embed_url")
    private String embed_url;

    @SerializedName("username")
    private String username;

    @SerializedName("source")
    private String source;

    @SerializedName("rating")
    private String rating;

    @SerializedName("caption")
    private String caption;

    @SerializedName("content_url")
    private String content_url;

    @SerializedName("source_tld")
    private String source_tld;

    @SerializedName("source_post_url")
    private String source_post_url;

    @SerializedName("import_datetime")
    private String import_datetime;

    @SerializedName("trending_datetime")
    private String trending_datetime;

    @SerializedName("images")
    private Images images;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getSlug() {
        return slug;
    }

    public String getUrl() {
        return url;
    }

    public String getBitly_gif_url() {
        return bitly_gif_url;
    }

    public String getBitly_url() {
        return bitly_url;
    }

    public String getEmbed_url() {
        return embed_url;
    }

    public String getUsername() {
        return username;
    }

    public String getSource() {
        return source;
    }

    public String getRating() {
        return rating;
    }

    public String getCaption() {
        return caption;
    }

    public String getContent_url() {
        return content_url;
    }

    public String getSource_tld() {
        return source_tld;
    }

    public String getSource_post_url() {
        return source_post_url;
    }

    public String getImport_datetime() {
        return import_datetime;
    }

    public String getTrending_datetime() {
        return trending_datetime;
    }

    public Images getImages() {
        return images;
    }
}
